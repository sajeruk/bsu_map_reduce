import sys

global_count = 0
global_key = ""

for line in sys.stdin.readlines():
    key, value = line.split('\t')
    global_count += int(value)
    global_key = key

print(global_key, global_count, sep='\t')
